import { Component, OnInit } from '@angular/core';
import { VcnService } from '../../vcn.service';
import { Options } from 'ng5-slider';
import 'hammerjs';
import { VcnSchedule, Vcnconfig } from 'src/app/vcnconfig';


@Component({
  selector: 'home-vms',
  templateUrl: './vms.component.html',
  styles: []
})
export class VmsComponent implements OnInit {

  level = 2;
  vcnfactoryconfig: Vcnconfig = this.vcnService.vcn;
  destroy;
  vcnschedule: VcnSchedule;
  date: Date;
  action = "apply";
  mdOpen = false;

  options: Options = {
    floor: 0,
    ceil: 100
  };

  single = [];
  updateChart() {
    this.single = [
      {
        "name": "Availaibility",
        "series": [
          {
            "value": 100,
            "name": "Microseg"
          },
          {
            "value": 100,
            "name": "AppDefense"
          },
          {
            "value": 20 + (Number(this.vcnfactoryconfig.vm) * 80),
            "name": "vSphere"
          },
          {
            "value": 20 + (Number(this.vcnfactoryconfig.vrni) * 80),
            "name": "vRNI"
          },
          {
            "value": 20 + (Number(this.vcnfactoryconfig.wan) * 80),
            "name": "SDWAN"
          },
          {
            "name": "SDN",
            "value": 20 + (Number(this.vcnfactoryconfig.networks) * 80)
          },
          {
            "name": "Bandwidth",
            "value": 100
          }
        ]
      },
      {
        "name": "Exposure Surface",
        "series": [
          {
            "value": 100 - (Number(this.vcnfactoryconfig.security) * 80),
            "name": "Microseg"
          },
          {
            "value": 100 - (Number(this.vcnfactoryconfig.appdefense) * 80),
            "name": "AppDefense"
          },
          {
            "value": 20,
            "name": "vRNI"
          },
          {
            "value": 20,
            "name": "SDWAN"
          },
          {
            "name": "SDN",
            "value": 20
          }
        ]
      }
    ]
  }

  // graph options
  view: any[] = [400, 400];
  showXAxis = true;
  showYAxis = false;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  showYAxisLabel = true;

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };
  
  constructor(public vcnService: VcnService) { }
/*
  getVcnConfig(): void {
    this.vcnService.getVcnConfig().subscribe(vcnfactoryconfig => {this.vcnfactoryconfig = vcnfactoryconfig;});
  }
*/

  getVcnChanges(): void {
    this.updateChart()
    this.vcnService.followVcnChanges(this.vcnfactoryconfig);
  }

  postVcnConfig(): void {
    console.log('Apply Vcn config');
    this.vcnService.vms_type = "success";
    this.vcnfactoryconfig.quarantine = 0;
    this.vcnService.postVcnConfig(this.vcnfactoryconfig).subscribe(() => console.log('Vcn config applied'));
    this.vcnService.followVcnProgress(this.vcnfactoryconfig);
  }

  destroyVcn(): void {
    console.log('Destroy Vcn config');
    this.vcnService.vms_type = "danger";
    this.vcnfactoryconfig.quarantine = 0;
    this.vcnService.vcnfactorychanges.changes=20; // Fake
    this.vcnService.destroyVcn().subscribe(() => console.log('Vcn config applied'));
    this.vcnService.followVcnProgress(this.vcnfactoryconfig);
  }

  ngOnInit() {
    //this.getVcnConfig();
    this.updateChart();
    if (!this.vcnService.vms_deploying)
      this.getVcnChanges();
  }

  onSchedule() {
    console.log("schedule");
    console.log(this.date);
    this.vcnschedule = {
      id: 1,
      action: this.action,
      date: this.date,
      completed: false,
      config: this.vcnfactoryconfig,
    }

    this.vcnService.scheduleVcnConfig(this.vcnschedule).subscribe(() => console.log('Vcn config scheduled'));
  }

}
