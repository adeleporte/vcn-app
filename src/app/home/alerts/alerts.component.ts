import { Component, OnInit } from '@angular/core';
import { VcnService } from '../../vcn.service';
import { Vcnalert } from '../../vcnconfig';

@Component({
  selector: 'home-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.css']
})
export class AlertsComponent implements OnInit {

  constructor(public vcnService: VcnService) { }

  listalerts: Vcnalert[];

  quarantine;
  quarantined = false;
  index: 0;

  quarantineVcn(): void {
    this.vcnService.quarantineVcn(this.index).subscribe();
  }

  ngOnInit() {}

}
