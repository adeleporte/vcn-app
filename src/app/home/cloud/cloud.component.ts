import { Component, OnInit } from '@angular/core';
import { Vcnconfig, VcnSchedule } from '../../vcnconfig';
import { VcnService } from '../../vcn.service';
import { Options } from 'ng5-slider';
import 'hammerjs';

@Component({
  selector: 'home-cloud',
  templateUrl: './cloud.component.html',
  styles: []
})
export class CloudComponent implements OnInit {

  options: Options = {
    floor: 0,
    ceil: 100
  };

  mdOpen = false;
  vcncloudconfig: Vcnconfig;
  vcncloudschedule: VcnSchedule;
  date: Date;
  destroy;
  action = "apply";

  constructor(public vcnService: VcnService) { }

  getVcnConfig(): void {
    this.vcnService.getVcnCloudConfig().subscribe(vcncloudconfig => {this.vcncloudconfig = vcncloudconfig;});
  }

  getVcnChanges(): void {
    this.vcnService.followVcnCloudChanges(this.vcncloudconfig);
  }

  postVcnConfig(): void {
    console.log('Apply Vcn config');
    this.vcnService.cloud_type = "success";
    this.vcncloudconfig.quarantine = 0;
    this.vcnService.postVcnCloudConfig(this.vcncloudconfig).subscribe(() => console.log('Vcn Cloud config applied'));
    this.vcnService.followVcnCloudProgress(this.vcncloudconfig);
  }

  destroyVcn(): void {
    console.log('Destroy CloudVcn config');
    this.vcnService.cloud_type = "danger";
    this.vcncloudconfig.quarantine = 0;
    this.vcnService.vcncloudchanges.changes=20; // Fake
    this.vcnService.destroyCloudVcn().subscribe(() => console.log('Vcn cloud config applied'));
    this.vcnService.followVcnCloudProgress(this.vcncloudconfig);
  }

  ngOnInit() {
    this.getVcnConfig();
    if (!this.vcnService.cloud_deploying)
      this.getVcnChanges();
  }

  onSchedule() {
    console.log("schedule");
    console.log(this.date);
    this.vcncloudschedule = {
      id: 1,
      action: 'apply',
      date: this.date,
      completed: false,
      config: this.vcncloudconfig,
    }

    this.vcnService.scheduleVcnConfig(this.vcncloudschedule).subscribe(() => console.log('Cloud Vcn config scheduled'));
  }

}
