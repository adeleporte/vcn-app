import {Component, OnInit, ViewChild} from "@angular/core";
import {ClrWizard} from "@clr/angular";
import { VcnService } from '../../vcn.service';

@Component({
  selector: 'app-build',
  templateUrl: './build.component.html',
  styleUrls: ['./build.component.css']
})
export class BuildComponent implements OnInit {

  constructor(private vcnService: VcnService) { }

  @ViewChild("wizardmd", {static: true}) wizardMedium: ClrWizard;
  @ViewChild("wizardlg", {static: true}) wizardLarge: ClrWizard;
  @ViewChild("wizardxl", {static: true}) wizardExtraLarge: ClrWizard;

  mdOpen: boolean = false;
  lgOpen: boolean = false;
  xlOpen: boolean = false;

  input;
  options;

  fakeOn() {
    console.log("fake on");
    this.vcnService.fakeOn().subscribe();
  }

  fakeOff() {
    console.log("fake off");
    this.vcnService.fakeOff().subscribe();
  }

  ngOnInit() {}


}
