import { Component, OnInit, AfterViewInit } from '@angular/core';
import { VcnService } from '../../vcn.service';
import * as cytoscape from 'cytoscape';
import { interval } from 'rxjs/internal/observable/interval';
import { startWith, switchMap } from 'rxjs/operators';
import { Options } from 'ng5-slider';
import cxtmenu from 'cytoscape-cxtmenu';
import tippy from 'tippy.js';
import 'tippy.js/dist/tippy.css';
import popper from 'cytoscape-popper';
import 'tippy.js/themes/light.css';

cytoscape.use( cxtmenu );

@Component({
  styleUrls: ['./flows.component.css'],
  selector: 'app-flows',
  templateUrl: './flows.component.html'
})
export class FlowsComponent implements OnInit, AfterViewInit {

  options: Options = {
    floor: 0,
    ceil: 1000,
    showTicks: true,

  };

  Loading = false;
  nb_flows = 100;

  VcnFlows: [{
    srcPort?: number;
    dstPort?: number;
    srcFlowAction?: string;
    dstFlowAction?: string;
    srcIps?: [string];
    dstIps?: [string];
    srcDefaultRuleHit?: boolean;
    dstDefaultRuleHit?: boolean;
    srcTxPackets?: number;
    srcRxPackets?: number;
    dstTxPackets?: number;
    dstRxPackets?: number;
    lastStartTime?: number;
    lastEndTime?: number;

  }] = [{
    srcPort: 0,
    dstPort: 0,
    srcFlowAction: 'ALLOW',
    dstFlowAction: 'ALLOW',
    srcIps: [''],
    dstIps: [''],
    srcDefaultRuleHit: false,
    dstDefaultRuleHit: false,
    srcRxPackets: 0,
    srcTxPackets: 0,
    dstRxPackets: 0,
    dstTxPackets: 0,
    lastEndTime: 0,
    lastStartTime: 0
  }];

  cy = cytoscape();





  constructor(public vcnService: VcnService) {
  }

  Refresh(): void {

    console.log('refresh');
    console.log(document.getElementsByClassName('cytograph').item(0));
    console.log(document.getElementsByName('cy'));
    console.log(document.getElementById('cy.cytograph'));


    const options = {
      name: 'circle',

      fit: true, // whether to fit to viewport
      padding: 30, // fit padding
      boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
      animate: true, // whether to transition the node positions
      animationDuration: 3000, // duration of animation in ms if enabled
      animationEasing: undefined, // easing of animation if enabled
      animateFilter ( node, i ) { return false; }, // a function that determines whether the node should be animated.  All nodes animated by default on animate enabled.  Non-animated nodes are positioned immediately when the layout starts
      ready: undefined, // callback on layoutready
      stop: undefined, // callback on layoutstop
      transform (node, position ) { return position; } // transform a given node position. Useful for changing flow direction in discrete layouts
    };
/*
    const instance = tippy('#cy', {
      content: 'Tooltip',
    });*/
    // instance[0].disable();
/*
    this.cy.nodes().on('select', n => {
      const ele = n.target;

      console.log(n);
    });
*/

    let defaults = {
  menuRadius: 100, // the radius of the circular menu in pixels
  selector: 'node', // elements matching this Cytoscape.js selector will trigger cxtmenus
  commands: [ // an array of commands to list in the menu or a function that returns the array
    /*
    { // example command
      fillColor: 'rgba(200, 200, 200, 0.75)', // optional: custom background color for item
      content: 'a command name' // html/text content to be displayed in the menu
      contentStyle: {}, // css key:value pairs to set the command's css in js if you want
      select: function(ele){ // a function to execute when the command is selected
        console.log( ele.id() ) // `ele` holds the reference to the active element
      },
      enabled: true // whether the command is selectable
    }
    */
  ], // function( ele ){ return [ /*...*/ ] }, // a function that returns commands or a promise of commands
  fillColor: 'rgba(0, 0, 0, 0.75)', // the background colour of the menu
  activeFillColor: 'rgba(1, 105, 217, 0.75)', // the colour used to indicate the selected command
  activePadding: 20, // additional size in pixels for the active command
  indicatorSize: 24, // the size in pixels of the pointer to the active command
  separatorWidth: 3, // the empty spacing in pixels between successive commands
  spotlightPadding: 4, // extra spacing in pixels between the element and the spotlight
  minSpotlightRadius: 24, // the minimum radius in pixels of the spotlight
  maxSpotlightRadius: 38, // the maximum radius in pixels of the spotlight
  openMenuEvents: 'cxttapstart taphold', // space-separated cytoscape events that will open the menu; only `cxttapstart` and/or `taphold` work here
  itemColor: 'white', // the colour of text in the command's content
  itemTextShadowColor: 'transparent', // the text shadow colour of the command's content
  zIndex: 9999, // the z-index of the ui div
  atMouse: false // draw menu at mouse position
};

    //let menu = this.cy.cxtmenu( defaults );

    this.cy.nodes().on('grab', n => {
      const ele = n.target;
      ele.connectedEdges().style({ opacity: 1 });
      /*
      instance[0].enable();
      instance[0].setProps({
        arrow: true,
        theme: 'tomato',
        animation: 'scale',
        lazy: true,
        interactive: true,
        trigger: 'manual',
        onShow(test) {
          test.popperInstance.reference = ele.popperRef();
          /*test.popperInstance.reference = {
            clientWidth: 20,
            clientHeight: 20,
            getBoundingClientRect() {
              return {
                // ...
                height: 10,
                width: 10,
                x: n.position.x,
                y: n.position.y,
                bottom: n.position.y,
                left: n.position.x,
                right: n.position.x+20,
                top: n.position.y-20
              };
          }
        }
      }});
      instance[0].setContent('<strong>Set Quarantine</strong>');
      instance[0].show();
*/
      console.log(n);
    });

    this.cy.nodes().on('free', n => {
      const ele = n.target;
      // instance[0].hide();
      ele.connectedEdges().style({ opacity: 0.4 });
    });


    const layout = this.cy.layout( options );
    layout.run();


  }

  ngOnInit() {
    this.VcnFlows.length = 1;
  }

  ngAfterViewInit() {
    console.log('content init');


    this.cy = cytoscape({ container: document.getElementById('cy'),
    style: [ // the stylesheet for the graph
      {
        selector: 'node',
        style: {
          'border-width': 2,
          'border-color': '#000000',
          label: 'data(id)',
          'font-size': 8,
          'min-zoomed-font-size': 4,
          // 'width': 60,
          // 'height': 30,
          // 'shape': 'circle',
          'text-halign': 'center',
          'text-valign': 'center',
          'background-color': '#555',
          'text-outline-color': '#555',
          'text-outline-width': '2px',
          color: '#fff',
          'overlay-padding': '6px',
          'z-index': 10,
          width: 'mapData(weight, 0, 10000, 20, 60)',
          height: 'mapData(weight, 0, 10000, 20, 60)'
        }
      },

      {
        selector: 'node:selected',
        style: {
          'border-width': '6px',
          'border-color': '#AAD8FF',
          'border-opacity': 0.5,
          'background-color': '#77828C',
          'text-outline-color': '#77828C'
        }
      },

      {
        selector: 'edge',
        style: {
          'curve-style': 'haystack',
          'haystack-radius': 0.5,
          opacity: 0.4,
          'line-color': '#bbb',
          width: 2,
          'overlay-padding': '3px'
        }
      },

      {
        selector: 'edge[pro = 0]',
        style: {
          'line-color': '#00FF00',
          'target-arrow-color': '#00FF00',
          'target-arrow-shape': 'triangle',
          label: 'data(label)',
          color: '#FFFFFF',
          'font-size': 5,
          // 'text-rotation': 'autorotate',
          // 'control-point-step-size': 3
        }
      },

      {
        selector: '[pro = 1]',
        style: {
          'line-color': 'red',
          width: 5,
          'target-arrow-color': 'red',
          'target-arrow-shape': 'triangle',
          label: 'data(label)',
          color: '#FFFFFF',
          'font-size': 5,
          // 'text-rotation': 'autorotate',
          // 'control-point-step-size': 3
        }
      }
    ],

    layout: {
      name: 'circle'
    }
    });

    this.cy.layout({
      name: 'circle',
      fit: true,
      animate: true
    }).run();

    this.vcnService.getVcnFlows().subscribe(flows => {
      this.VcnFlows = flows;

      const result1 = Array.from(new Set(this.VcnFlows.map(x => x.srcIps[0])));
      console.log(result1);

      const result2 = Array.from(new Set(this.VcnFlows.map(x => x.dstIps[0])));
      console.log(result2);

      const result12 = result1.concat(result2);
      console.log(result12);
      const unique = [new Set(result12)];

      console.log(unique);

      const collection = this.cy.elements();
      this.cy.remove( collection );

      unique[0].forEach(element => {
        if (element.toString() == '255.255.255.255') {
          element = 'UNKNOWN';
        }
        this.cy.add({group: 'nodes', data: { id: element.toString(), name: element.toString()/*, position: { x: Math.random()*600, y: Math.random()*600 }*/, weight: Math.random() * 10000 }});
      });

      this.VcnFlows.forEach(element => {
        let Fprotected = 1;
        if (element.srcDefaultRuleHit && element.dstDefaultRuleHit) {
          Fprotected = 0;
        }
        if (element.srcIps[0] === '255.255.255.255') {
            element.srcIps[0] = 'UNKNOWN';
        }
        if (element.dstIps[0] === '255.255.255.255') {
          element.dstIps[0] = 'UNKNOWN';
      }
        console.log(Fprotected.valueOf());
        this.cy.add({group: 'edges',  data: { id: Math.random().toString(), source: element.srcIps[0], target: element.dstIps[0], label: element.dstPort.toString(), pro: Fprotected }});
      });

      this.Refresh();

    });
  }

  StartCapture(): void {
    console.log('start');
    const subscription = interval(3000).pipe(startWith(0), switchMap(() => this.vcnService.getVcnFlows())).subscribe(flows => {
      this.VcnFlows = flows;

      const result1 = Array.from(new Set(this.VcnFlows.map(x => x.srcIps[0])));
      console.log(result1);

      const result2 = Array.from(new Set(this.VcnFlows.map(x => x.dstIps[0])));
      console.log(result2);

      const result12 = result1.concat(result2);
      const unique = [new Set(result12)];

      console.log(unique);

      this.cy.json({elements: {}});
      unique[0].forEach(element => {
        this.cy.add({group: 'nodes', data: { id: element.toString(), name: element.toString(), position: { x: Math.random() * 600, y: Math.random() * 600 }, weight: Math.random() * 10000 }});
      });
      console.log(this.cy.json());

      this.VcnFlows.forEach(element => {
        let Fprotected = 1;
        if (element.srcDefaultRuleHit && element.dstDefaultRuleHit) {
          Fprotected = 0;
        }
        let size = element.dstTxPackets;
        let mid = Math.random().toString();
        this.cy.add({group: 'edges',  data: { id: mid, source: element.srcIps[0], target: element.dstIps[0], label: element.dstPort.toString(), pro: Fprotected, weight: size }});
      });

      console.log(this.cy.json());

      this.Refresh();
    });
  }

  StopCapture(): void {
    console.log('stop');
    this.vcnService.stopVcnFlows().subscribe();
  }

  Test(): void {
    console.log('test');

    this.cy.add({group: 'nodes', data: { id: Math.random().toString(), name: Math.random().toString(), position: { x: Math.random() * 600, y: Math.random() * 600 } }});
  }

  LayoutCose(): void {
    console.log('cose layout');

    const options = {
      name: 'cose',

      // Called on `layoutready`
      ready() {},

      // Called on `layoutstop`
      stop() {},

      // Whether to animate while running the layout
      // true : Animate continuously as the layout is running
      // false : Just show the end result
      // 'end' : Animate with the end result, from the initial positions to the end positions
      animate: 'end',

      // Easing of the animation for animate:'end'
      animationEasing: undefined,

      // The duration of the animation for animate:'end'
      animationDuration: 3000,

      // A function that determines whether the node should be animated
      // All nodes animated by default on animate enabled
      // Non-animated nodes are positioned immediately when the layout starts
      animateFilter ( node, i ) { return true; },


      // The layout animates only after this many milliseconds for animate:true
      // (prevents flashing on fast runs)
      animationThreshold: 250,

      // Number of iterations between consecutive screen positions update
      refresh: 20,

      // Whether to fit the network view after when done
      fit: true,

      // Padding on fit
      padding: 30,

      // Constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
      boundingBox: undefined,

      // Excludes the label when calculating node bounding boxes for the layout algorithm
      nodeDimensionsIncludeLabels: false,

      // Randomize the initial positions of the nodes (true) or use existing positions (false)
      randomize: false,

      // Extra spacing between components in non-compound graphs
      componentSpacing: 80,

      // Node repulsion (non overlapping) multiplier
      nodeRepulsion( node ) { return 2048; },

      // Node repulsion (overlapping) multiplier
      nodeOverlap: 4,

      // Ideal edge (non nested) length
      idealEdgeLength( edge ) { return 32; },

      // Divisor to compute edge forces
      edgeElasticity( edge ) { return 32; },

      // Nesting factor (multiplier) to compute ideal edge length for nested edges
      nestingFactor: 1.2,

      // Gravity force (constant)
      gravity: 1,

      // Maximum number of iterations to perform
      numIter: 1000,

      // Initial temperature (maximum node displacement)
      initialTemp: 1000,

      // Cooling factor (how the temperature is reduced between consecutive iterations
      coolingFactor: 0.99,

      // Lower temperature threshold (below this point the layout will end)
      minTemp: 1.0
    };

    let layout = this.cy.layout( options );
    layout.run();
  }

  LayoutCircle(): void {
    console.log('circle');
    const options = {
      name: 'circle',

      fit: true, // whether to fit the viewport to the graph
      padding: 30, // the padding on fit
      boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
      avoidOverlap: true, // prevents node overlap, may overflow boundingBox and radius if not enough space
      nodeDimensionsIncludeLabels: false, // Excludes the label when calculating node bounding boxes for the layout algorithm
      spacingFactor: undefined, // Applies a multiplicative factor (>0) to expand or compress the overall area that the nodes take up
      radius: undefined, // the radius of the circle
      startAngle: 3 / 2 * Math.PI, // where nodes start in radians
      sweep: undefined, // how many radians should be between the first and last node (defaults to full circle)
      clockwise: true, // whether the layout should go clockwise (true) or counterclockwise/anticlockwise (false)
      sort: undefined, // a sorting function to order the nodes; e.g. function(a, b){ return a.data('weight') - b.data('weight') }
      animate: true, // whether to transition the node positions
      animationDuration: 3000, // duration of animation in ms if enabled
      animationEasing: undefined, // easing of animation if enabled
      animateFilter ( node, i ) { return true; }, // a function that determines whether the node should be animated.  All nodes animated by default on animate enabled.  Non-animated nodes are positioned immediately when the layout starts
      ready: undefined, // callback on layoutready
      stop: undefined, // callback on layoutstop
      transform (node, position ) { return position; } // transform a given node position. Useful for changing flow direction in discrete layouts

    };

    let layout = this.cy.layout( options );
    layout.run();
  }

  LayoutGrid(): void {
    console.log('grid');
    const options = {
      name: 'concentric',

      fit: true, // whether to fit the viewport to the graph
      padding: 30, // the padding on fit
      startAngle: 3 / 2 * Math.PI, // where nodes start in radians
      sweep: undefined, // how many radians should be between the first and last node (defaults to full circle)
      clockwise: true, // whether the layout should go clockwise (true) or counterclockwise/anticlockwise (false)
      equidistant: false, // whether levels have an equal radial distance betwen them, may cause bounding box overflow
      minNodeSpacing: 10, // min spacing between outside of nodes (used for radius adjustment)
      boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
      avoidOverlap: true, // prevents node overlap, may overflow boundingBox if not enough space
      nodeDimensionsIncludeLabels: false, // Excludes the label when calculating node bounding boxes for the layout algorithm
      height: undefined, // height of layout area (overrides container height)
      width: undefined, // width of layout area (overrides container width)
      spacingFactor: undefined, // Applies a multiplicative factor (>0) to expand or compress the overall area that the nodes take up
      concentric( node ) { // returns numeric value for each node, placing higher nodes in levels towards the centre
      return node.degree();
      },
      levelWidth( nodes ) { // the letiation of concentric values in each level
      return nodes.maxDegree() / 4;
      },
      animate: true, // whether to transition the node positions
      animationDuration: 3000, // duration of animation in ms if enabled
      animationEasing: undefined, // easing of animation if enabled
      animateFilter ( node, i ) { return true; }, // a function that determines whether the node should be animated.  All nodes animated by default on animate enabled.  Non-animated nodes are positioned immediately when the layout starts
      ready: undefined, // callback on layoutready
      stop: undefined, // callback on layoutstop
      transform (node, position ) { return position; } // transform a given node position. Useful for changing flow direction in discrete layouts
    };

    this.cy.layout( options ).run();
  }

  LayoutRandom(): void {
    console.log('random');
    const options = {
      name: 'random',

      fit: true, // whether to fit the viewport to the graph
      directed: false, // whether the tree is directed downwards (or edges can point in any direction if false)
      padding: 30, // padding on fit
      circle: false, // put depths in concentric circles if true, put depths top down if false
      grid: false, // whether to create an even grid into which the DAG is placed (circle:false only)
      spacingFactor: 1.75, // positive spacing factor, larger => more space between nodes (N.B. n/a if causes overlap)
      boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
      avoidOverlap: true, // prevents node overlap, may overflow boundingBox if not enough space
      nodeDimensionsIncludeLabels: false, // Excludes the label when calculating node bounding boxes for the layout algorithm
      roots: undefined, // the roots of the trees
      maximal: false, // whether to shift nodes down their natural BFS depths in order to avoid upwards edges (DAGS only)
      animate: true, // whether to transition the node positions
      animationDuration: 3000, // duration of animation in ms if enabled
      animationEasing: undefined, // easing of animation if enabled,
      animateFilter ( node, i ) { return true; }, // a function that determines whether the node should be animated.  All nodes animated by default on animate enabled.  Non-animated nodes are positioned immediately when the layout starts
      ready: undefined, // callback on layoutready
      stop: undefined, // callback on layoutstop
      transform (node, position ) { return position; } // transform a given node position. Useful for changing flow direction in discrete layouts
    };

    this.cy.layout( options ).run();
  }

}
