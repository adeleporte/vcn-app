import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { FormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';

import { ClarityModule } from '@clr/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ROUTING } from "../app.routing";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FormsModule,
    ClarityModule,
    BrowserAnimationsModule,
    ROUTING,
    AgmCoreModule
  ]
})
export class HomeModule { }
