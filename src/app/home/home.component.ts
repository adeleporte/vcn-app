import { Component, OnInit } from '@angular/core';
import { VcnService } from '../vcn.service';
import { AuthenticationService } from '../authentication.service';
import { Vcnconfig, Vcnalerts, Vcnalert, Vcnedge } from '../vcnconfig';
import { of, interval, empty} from "rxjs";
import {startWith, switchMap, flatMap, mergeMap, catchError} from "rxjs/operators";
import { AngularWaitBarrier } from 'blocking-proxy/built/lib/angular_wait_barrier';
import { forEach } from '@angular/router/src/utils/collection';
import { Items } from '@clr/angular/data/datagrid/providers/items';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  title = 'vcn-app';
  loading = false;

  
  vcnalert: Vcnalert = {
    id: 1,
    ruleViolated: "OUTBOUND_CONNECTION",
    severity: "CRITICAL",
    violationDetails: {cli: "", processPath: "", localAddress: "", remoteAddress: "", localPort: "", remotePort: ""},
    quarantine: false
  };

  vcnalerts: Vcnalerts = {
    totalElements: 0,
    members: [this.vcnalert]
  };

  vcnsdwanconfig: Vcnconfig = {
    id: "factory",
    application: "yelb",
    site: "paris",
    vm: true,
    networks: true,
    security: true,
    appdefense: true,
    vrni: true,
    wan: true,
    bandwidth: 100,
    token: "123",
    pks: "",
    quarantine: 0,
    region: "frankfurt",
    sddc: "frankfurt"
  };

  vcnedges: Vcnedge[] = [];

  info;
  user;
  
  constructor(private auth: AuthenticationService, public vcnService: VcnService) { }


  ngOnInit() {

      this.getVcnEdges();
      this.user = this.auth.getUserDetails()
      console.log(this.user)

      interval(5000).pipe(
        flatMap(num => of(num).pipe(
          mergeMap(num => this.vcnService.getAlerts()),
          catchError((error) => {
            console.log("error");
            return empty();
          })
        ))
      ).subscribe(vcnalerts => {
        this.vcnalerts = vcnalerts;
        // Copy the quanrantine tag
        //this.vcnalerts.members[].quarantine = this.vcnService.quarantine[];
        //this.vcnalerts.members.forEach(function (value, index) {console.log(value); console.log(index); value.quarantine = true })
        if (this.vcnalerts.totalElements != 0)
        {
          // Alert
          this.vcnService.notify('AppDefense Alert: ' + this.vcnalerts.members[0].violationDetails.cli);
        }
        
        this.vcnService.cacheVcnAlerts(this.vcnalerts.members);
        
      })
  }

  getVcnEdges(): void {
    this.loading = true;
    this.vcnService.getVcnEdges(this.vcnsdwanconfig).subscribe(vcnedges => {this.vcnedges = vcnedges; this.cacheVcnEdges(this.vcnedges); this.loading = false});
  }

  cacheVcnEdges(vcnedges: Vcnedge[]) {
    this.vcnService.cacheVcnEdges(vcnedges);
  }


}
