import { Component,  EventEmitter, Input, Output, OnInit } from '@angular/core';
import { VcnService } from '../../vcn.service';
import { Vcnedge, Vcnconfig } from '../../vcnconfig';
//import { HomeComponent } from '../home.component';
//import { MapTypeControlStyle } from '@agm/core/services/google-maps-types';


@Component({
  selector: 'app-sdwan',
  templateUrl: './sdwan.component.html',
  styleUrls: ['./sdwan.component.css']
})


export class SdwanComponent implements OnInit {

  vcnsdwanconfig: Vcnconfig = {
    id: "factory",
    application: "yelb",
    site: "paris",
    vm: true,
    networks: true,
    security: true,
    appdefense: true,
    vrni: true,
    wan: true,
    bandwidth: 100,
    token: "123",
    pks: "",
    quarantine: 0,
    region: "frankfurt",
    sddc: "frankfurt"
  };

  lat: number = 48.866667;
  lng: number = 2.333333;
  zoom: number = 2;

  latstart: string = "48.866667";
  lngstart: string = "48.866667";
  latend: string = "48.866667";
  lngend:string = "48.866667";

  vcnedges: Vcnedge[] = [];
  selected_edge: Vcnedge;

  icon={
    url: 'assets/outline-vcn.png',
    scaledSize: {
        width: 20,
        height: 20
    }
  }

  vmcicon={
    url: 'assets/outline-vmc.png',
    scaledSize: {
        width: 30,
        height: 30
    }
  }

  styles: [{
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#212121"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#212121"
      }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "administrative.country",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "administrative.locality",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#181818"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#1b1b1b"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#2c2c2c"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#8a8a8a"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#373737"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#3c3c3c"
      }
    ]
  },
  {
    "featureType": "road.highway.controlled_access",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#4e4e4e"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#000000"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#3d3d3d"
      }
    ]
  }] = [
    {
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#212121"
        }
      ]
    },
    {
      "elementType": "labels.icon",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#757575"
        }
      ]
    },
    {
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#212121"
        }
      ]
    },
    {
      "featureType": "administrative",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#757575"
        }
      ]
    },
    {
      "featureType": "administrative.country",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#9e9e9e"
        }
      ]
    },
    {
      "featureType": "administrative.locality",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#bdbdbd"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#757575"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#181818"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#616161"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#1b1b1b"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#2c2c2c"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#8a8a8a"
        }
      ]
    },
    {
      "featureType": "road.arterial",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#373737"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#3c3c3c"
        }
      ]
    },
    {
      "featureType": "road.highway.controlled_access",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#4e4e4e"
        }
      ]
    },
    {
      "featureType": "road.local",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#616161"
        }
      ]
    },
    {
      "featureType": "transit",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#757575"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#000000"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#3d3d3d"
        }
      ]
    }
  ]

  selectedMarker;
  move;

  constructor(private vcnService: VcnService) { }

  selectMarker(event) {
    this.selectedMarker = {
      lat: event.latitude,
      lng: event.longitude
    };
    console.log(this.selectedMarker);
  }

  getVcnEdges(): void {
    //this.vcnService.getVcnEdges(this.vcnsdwanconfig).subscribe(vcnedges => this.vcnedges = vcnedges);
    this.vcnedges = this.vcnService.getCachedVcnEdges();
  }

  migrateHcx(): void {
    //this.vcnService.getVcnEdges(this.vcnsdwanconfig).subscribe(vcnedges => this.vcnedges = vcnedges);
    console.log("Migrate to HCX");
    this.vcnService.migrateHcx().subscribe();
  }

  ngOnInit() {
    this.getVcnEdges();
  }

  markerDrag(event, edge: Vcnedge) {
    console.log("drag");
    console.log(edge);
    console.log(event);
  }

  markerDragStart(event, edge: Vcnedge) {
    console.log("start");
    console.log(event);
    console.log(edge);
    this.latend = event.latitude;
    this.lngend = event.longitude;
  }

  markerDragEnd(event, edge: Vcnedge) {
    console.log("end");
    this.selected_edge = edge;
    console.log(edge);
    console.log(event);
  }

}
