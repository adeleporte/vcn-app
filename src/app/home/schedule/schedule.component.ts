import { Component, OnInit } from '@angular/core';
import { VcnService } from '../../vcn.service';
import { VcnSchedule } from '../../vcnconfig';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit {

  constructor(public vcnService: VcnService) { }

  vcnschedules: VcnSchedule[];
  schedule_to_delete: '';
  delete = false;

  ngOnInit() {
    this.vcnService.getVcnSchedule().subscribe(vcnschedules => this.vcnschedules = vcnschedules);
  }

  cancelSchedule() {
    this.vcnService.cancelVcnSchedule(this.schedule_to_delete).subscribe(vcnschedules => this.vcnschedules = vcnschedules);

    this.vcnService.getVcnSchedule().subscribe(vcnschedules => this.vcnschedules = vcnschedules);
  }

}
