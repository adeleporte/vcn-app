import { Component, OnInit } from '@angular/core';
import { Vcnconfig, VcnSchedule } from '../../vcnconfig';
import { VcnService } from '../../vcn.service';
import 'hammerjs';

//declare var cytoscape: any;


@Component({
  selector: 'home-containers',
  templateUrl: './containers.component.html',
  styleUrls: ['./containers.component.css']
})
export class ContainersComponent implements OnInit {

  vcnpksconfig: Vcnconfig;
  vcnpksschedule: VcnSchedule;
  date: Date;
  destroy;
  action = "apply";
  

  graphData = {
    nodes: [
        {data: {id: 'j', name: 'Jerry', faveColor: '#6FB1FC', faveShape: 'triangle'}},
        {data: {id: 'e', name: 'Elaine', faveColor: '#EDA1ED', faveShape: 'ellipse'}},
        {data: {id: 'k', name: 'Kramer', faveColor: '#86B342', faveShape: 'octagon'}},
        {data: {id: 'g', name: 'George', faveColor: '#F5A45D', faveShape: 'rectangle'}}
    ],
    edges: [
        {data: {source: 'j', target: 'e', faveColor: '#6FB1FC'}},
        {data: {source: 'j', target: 'k', faveColor: '#6FB1FC'}},
        {data: {source: 'j', target: 'g', faveColor: '#6FB1FC'}},

        {data: {source: 'e', target: 'j', faveColor: '#EDA1ED'}},
        {data: {source: 'e', target: 'k', faveColor: '#EDA1ED'}},

        {data: {source: 'k', target: 'j', faveColor: '#86B342'}},
        {data: {source: 'k', target: 'e', faveColor: '#86B342'}},
        {data: {source: 'k', target: 'g', faveColor: '#86B342'}},

        {data: {source: 'g', target: 'j', faveColor: '#F5A45D'}}
    ]
};

  mdOpen = false;

  constructor(public vcnService: VcnService) { }

  getVcnConfig(): void {
    this.vcnService.getVcnPksConfig().subscribe(vcnpksconfig => {this.vcnpksconfig = vcnpksconfig;});
  }

  getVcnChanges(): void {
    this.vcnService.followVcnPksChanges(this.vcnpksconfig);
  }

  postVcnConfig(): void {
    console.log('Apply Vcn config');
    this.vcnService.pks_type = "success";
    this.vcnpksconfig.quarantine = 0;
    this.vcnService.postVcnPksConfig(this.vcnpksconfig).subscribe(() => console.log('Vcn Pks config applied'));
    this.vcnService.followVcnPksProgress(this.vcnpksconfig);
  }

  destroyVcn(): void {
    console.log('Destroy Pks Vcn config');
    this.vcnService.pks_type = "danger";
    this.vcnpksconfig.quarantine = 0;
    this.vcnService.vcnpkschanges.changes=20; // Fake
    this.vcnService.destroyPksVcn().subscribe(() => console.log('Vcn pks config applied'));
    this.vcnService.followVcnPksProgress(this.vcnpksconfig);
  }

  ngOnInit() {

    this.getVcnConfig();
    if (!this.vcnService.pks_deploying)
      this.getVcnChanges();
  }

  onSchedule() {
    console.log("schedule");
    console.log(this.date);
    this.vcnpksschedule = {
      id: 1,
      action: 'apply',
      date: this.date,
      completed: false,
      config: this.vcnpksconfig,
    }

    this.vcnService.scheduleVcnConfig(this.vcnpksschedule).subscribe(() => console.log('Pks Vcn config scheduled'));
  }

}
