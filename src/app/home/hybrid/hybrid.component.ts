import { Component, OnInit } from '@angular/core';
import { VcnService } from '../../vcn.service';
import { VcnSchedule, Vcnconfig } from 'src/app/vcnconfig';

@Component({
  selector: 'app-hybrid',
  templateUrl: './hybrid.component.html',
  styleUrls: ['./hybrid.component.css']
})
export class HybridComponent implements OnInit {

  vcnhybridconfig: Vcnconfig = this.vcnService.vcnhybrid;
  destroy;
  vcnschedule: VcnSchedule;
  date: Date;
  action = "apply";
  mdOpen = false;


  constructor(public vcnService: VcnService) { }

  ngOnInit() {
  }

  getVcnChanges() {

  }

  postVcnConfig() {

  }

  destroyVcn() {

  }

  onSchedule() {

  }

}
