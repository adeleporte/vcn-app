import { Component, OnInit } from '@angular/core';
import { VcnService } from '../../vcn.service';
import { Vcnconfig, Vcnchanges, Vcnprogress } from '../../vcnconfig';
import {interval} from "rxjs/internal/observable/interval";
import {startWith, switchMap} from "rxjs/operators";

@Component({
  selector: 'home-vmc',
  templateUrl: './vmc.component.html',
  styleUrls: ['./vmc.component.css']
})
export class VmcComponent implements OnInit {


  vcnvmcconfig: Vcnconfig = {
    id: "factory",
    application: "testapp",
    site: "paris",
    vm: true,
    networks: true,
    security: true,
    appdefense: true,
    vrni: true,
    wan: true,
    bandwidth: 100,
    token: "123",
    pks: "",
    quarantine: 0,
    region: "frankfurt",
    sddc: "frankfurt"
  };
  vcnvmcchanges: Vcnchanges = {
    changes: 0,
    tocreate: 0,
    toupdate: 0,
    todelete: 0
  };
  vcnvmcprogress: Vcnprogress = {
    changes: 0,
    completed: false,
    lastline: "",
    fullline: ""
  };

  logs: string[] = [""];

  progress: string = "success";

  destroy;

  loading: boolean = false;

  constructor(private vcnService: VcnService) { }

  getVcnConfig(): void {
    this.loading = true;
    this.vcnService.getVcnVmcConfig().subscribe(vcnvmcconfig => {this.vcnvmcconfig = vcnvmcconfig; this.loading = false});
  }

  getVcnChanges(): void {
    this.loading = true;
    this.vcnService.getVcnVmcChanges(this.vcnvmcconfig).subscribe(vcnvmcchanges => {this.vcnvmcchanges = vcnvmcchanges; this.loading = false});
  }

  getVcnProgress(): void {
    this.vcnService.getVcnVmcProgress().subscribe(vcnvmcprogress => this.vcnvmcprogress = vcnvmcprogress);
  }

  postVcnConfig(): void {
    this.vcnvmcconfig.quarantine = 0;
    this.vcnService.postVcnVmcConfig(this.vcnvmcconfig).subscribe();
    const subscription = interval(1000).pipe(startWith(0), switchMap(() => this.vcnService.getVcnVmcProgress())).subscribe(
      {next: vcnvmcprogress => {
          this.vcnvmcprogress = vcnvmcprogress;
          if (this.logs[0] != this.vcnvmcprogress.lastline)
          {
            this.logs.unshift(this.vcnvmcprogress.lastline);
          };        
          if (this.vcnvmcprogress.completed)
          {
            this.logs.unshift(this.vcnvmcprogress.lastline);
            subscription.unsubscribe();this.vcnvmcprogress.changes = this.vcnvmcchanges.changes;
            this.getVcnChanges();
          }
        },
      error: err => console.error(err),
      complete: () => console.log('complete')
      });
  }


  destroyVcn(): void {
    console.log('Destroy Pks Vcn config');
    this.progress = "danger";
    this.vcnvmcconfig.quarantine = 0;
    this.vcnvmcchanges.changes=20; // Fake destroy value
    this.vcnService.destroyVmcVcn().subscribe(() => console.log('Vmc Vcn config applied'));
    const subscription = interval(1000).pipe(startWith(0), switchMap(() => this.vcnService.getVcnVmcProgress())).subscribe(
      {next: vcnvmcprogress => {
          this.vcnvmcprogress = vcnvmcprogress;
          if (this.logs[0] != this.vcnvmcprogress.lastline)
          {
            this.logs.unshift(this.vcnvmcprogress.lastline);
          };        
          if (this.vcnvmcprogress.completed)
          {
            this.logs.unshift(this.vcnvmcprogress.lastline);
            subscription.unsubscribe();this.vcnvmcprogress.changes = this.vcnvmcchanges.changes;
            this.getVcnChanges();
          }
        },
      error: err => console.error(err),
      complete: () => console.log('complete')
      });
  }

  ngOnInit() {

    this.getVcnConfig();
    this.getVcnChanges();
    //this.getVcnProgress();


  }

}
