import { Component, OnInit } from '@angular/core';
import { VcnService } from '../../vcn.service';
import { Stats } from 'fs';

@Component({
  selector: 'home-status',
  templateUrl: './status.component.html',
  styles: ['./status.component.css']
})


export class StatusComponent implements OnInit {
  

  view: any[] = [300, 300];

  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = false;
  xAxisLabel = 'Country';
  showYAxisLabel = false;
  yAxisLabel = 'Population';

  colorScheme = {
    domain: ['#bf9d76', '#e99450', '#d89f59', '#f2dfa7', '#a5d7c6', '#7794b1', '#afafaf', '#707160', '#ba9383', '#d9d5c3']
  };

  flame = {
    domain: ['#a8385d', '#7aa3e5', '#a27ea8', '#aae3f5', '#adcded', '#a95963', '#8796c0', '#7ed3ed', '#50abcc', '#ad6886']
  };
  

  single = [];
  methods = [];
  Loading = false;

  constructor(public vcnService: VcnService) {

   }

  UpdateChart() {

    this.single = [
      {
        "name": "VM",
        "value": this.vcnService.stats.countFactory
      },
      {
        "name": "PKS",
        "value": this.vcnService.stats.countPks
      },
      {
        "name": "AWS",
        "value": this.vcnService.stats.countCloud
      },
      {
        "name": "VMC",
        "value": this.vcnService.stats.countVMC
      },
    ]; 

    this.methods = [
      {
        "name": "Plan",
        "value": this.vcnService.stats.countPlan
      },
      {
        "name": "Apply",
        "value": this.vcnService.stats.countApply
      },
      {
        "name": "Destroy",
        "value": this.vcnService.stats.countDestroy
      },
    ]; 
  }

  ngOnInit() {
    this.getDashboard();
    this.getLogs();
  }

  getDashboard() {
    this.vcnService.getDashboard().subscribe(stats => {this.vcnService.stats = stats; this.UpdateChart();});
  }

  getLogs() {
    this.vcnService.getLogs().subscribe(logs => {this.vcnService.logs = logs;});
  }


}
