import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms'

import { AppComponent } from './app.component';
import { ClarityModule } from '@clr/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ROUTING } from "./app.routing";
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AgmCoreModule } from '@agm/core';
import { StatusComponent } from './home/status/status.component';
import { VmsComponent } from './home/vms/vms.component';
import { ContainersComponent } from './home/containers/containers.component';
import { CloudComponent } from './home/cloud/cloud.component';
import { VmcComponent } from './home/vmc/vmc.component';
import { AlertsComponent } from './home/alerts/alerts.component';
import { BuildComponent } from './home/build/build.component';
import { SdwanComponent } from './home/sdwan/sdwan.component';
import { HelpComponent } from './home/help/help.component';
import { ScheduleComponent } from './home/schedule/schedule.component';
import { AuthGuard } from "./guards/auth-guard.service";
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { Ng5SliderModule } from 'ng5-slider';
import { CytoscapeModule } from 'ngx-cytoscape';
import {MatSliderModule} from '@angular/material';
import { PushNotificationsModule } from 'ng-push';
import {CalendarModule} from 'primeng/calendar';
import { FlowsComponent } from './home/flows/flows.component';
import { HybridComponent } from './home/hybrid/hybrid.component'

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    StatusComponent,
    ContainersComponent,
    CloudComponent,
    VmcComponent,
    VmsComponent,
    AlertsComponent,
    HomeComponent,
    BuildComponent,
    ScheduleComponent,
    SdwanComponent,
    HelpComponent,
    FlowsComponent,
    HybridComponent
  ],
  imports: [
    BrowserModule,
    ClarityModule,
    BrowserAnimationsModule,
    NgxChartsModule,
    FormsModule,
    ROUTING,
    HttpClientModule,
    Ng5SliderModule,
    CytoscapeModule,
    MatSliderModule,
    PushNotificationsModule,
    CalendarModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDtgIUhdnQ1b2rm72j3AxxR4rMB7xH6hMQ'
   })
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
