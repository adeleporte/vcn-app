import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
//import { VcnService } from '../vcn.service';
//import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { VcnUser } from '../vcnconfig';
import { AuthenticationService } from '../authentication.service';


@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  credentials: VcnUser = {
    username: '',
    password: ''
  }

  error: boolean

  constructor(private auth: AuthenticationService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit() {
    console.log(this.credentials)

    this.auth.login(this.credentials).subscribe(authenticated => {
      if (authenticated)
      {
        this.error = false;
        // Save token
        this.auth.saveToken(authenticated.token)
        console.log(authenticated.token);
        this.router.navigateByUrl('/home/status');
      }           
    }, () => {
      this.error = true
      console.log('bad credentials');
    });
  }

}
