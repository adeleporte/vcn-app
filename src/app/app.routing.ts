import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { StatusComponent } from './home/status/status.component';
import { VmsComponent } from './home/vms/vms.component';
import { ContainersComponent } from './home/containers/containers.component';
import { CloudComponent } from './home/cloud/cloud.component';
import { ScheduleComponent } from './home/schedule/schedule.component';
import { VmcComponent } from './home/vmc/vmc.component';
import { AlertsComponent } from './home/alerts/alerts.component';
import { BuildComponent } from './home/build/build.component';
import { SdwanComponent } from './home/sdwan/sdwan.component';
import { HelpComponent } from './home/help/help.component';
import { FlowsComponent } from './home/flows/flows.component';
import { AuthGuard } from './guards/auth-guard.service';
import { HybridComponent } from './home/hybrid/hybrid.component';

export const ROUTES: Routes = [
    {path: '', redirectTo: 'login', pathMatch: 'prefix'},
    {path: 'login', component: LoginComponent},
    {path: 'home',
    component: HomeComponent,
    children: [
        {
            path: '',
            redirectTo: 'status',
            pathMatch: 'full'
        },
        {
            path: 'status',
            component: StatusComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'vms',
            component: VmsComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'containers',
            component: ContainersComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'hybrid',
            component: HybridComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'cloud',
            component: CloudComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'vmc',
            component: VmcComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'alerts',
            component: AlertsComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'schedule',
            component: ScheduleComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'sdwan',
            component: SdwanComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'flows',
            component: FlowsComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'build',
            component: BuildComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'help',
            component: HelpComponent
        }
    ]
    }
];

export const ROUTING: ModuleWithProviders = RouterModule.forRoot(ROUTES);
