import { Injectable } from '@angular/core';
import { Vcnconfig, Vcnedge, Vcnalert, VcnLogs, VcnStats, Vcnchanges, Vcnprogress, VcnSchedule } from './vcnconfig';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
//import { userInfo } from 'os';
import { interval } from "rxjs/internal/observable/interval";
import { startWith, switchMap } from "rxjs/operators";
import { PushNotificationsService} from 'ng-push';


@Injectable({
  providedIn: 'root'
})
export class VcnService {

  vcnedges: Vcnedge[] = [];
  vcnalerts: Vcnalert[] = [];
  quarantine: boolean[] = [];
  vcnschedules: VcnSchedule[] = [];

    vcn: Vcnconfig = {
      id: "factory",
      application: 'TESTAPP',
      site: 'Australia',
      vm: true,
      networks: true,
      security: true,
      appdefense: true,
      vrni: true,
      threshold: true,
      wan: true,
      bandwidth: 10,
      token: "123",
      quarantine: 0,
      class: "bulk",
      servicegroup: "ALL",
      avi: false,
      hcx: false,
      gslb: false,
      level: "testapp",
      waf: false
    }; 

    vcncloud: Vcnconfig = {
      id: "aws",
      application: 'TESTAPP',
      site: 'Australia',
      vm: true,
      networks: true,
      security: true,
      vrni: true,
      threshold: true,
      wan: true,
      bandwidth: 10,
      token: "123",
      quarantine: 0,
      region: "Frankfurt",
      class: "bulk",
      servicegroup: "ALL"
    }; 

    vcnpks: Vcnconfig = {
      id: "pks",
      application: 'TESTAPP',
      site: 'Australia',
      //vm: true,
      networks: true,
      security: true,
      //appdefense: true,
      vrni: true,
      wan: true,
      bandwidth: 10,
      //token: "123",
      //quarantine: 0,
      pks: "pks-france",
      //region: "Frankfurt",
      //sddc: "Frankfurt",
      replicas: 3,
      appversion: "v1"
    }; 

    vcnvmc: Vcnconfig = {
      id: "vmc",
      application: 'TESTAPP',
      site: 'Australia',
      vm: true,
      networks: true,
      security: true,
      vrni: true,
      wan: true,
      bandwidth: 10,
      token: "123",
      quarantine: 0,
      region: "Frankfurt",
      sddc: "Frankfurt"
    };

    vcnhybrid: Vcnconfig = {
      id: "factory",
      application: 'PIMPMYHOUSE',
      site: 'Australia',
      vm: true,
      networks: true,
      security: true,
      appdefense: true,
      vrni: true,
      threshold: true,
      wan: true,
      bandwidth: 10,
      token: "123",
      quarantine: 0,
      class: "bulk",
      servicegroup: "ALL",
      avi: false,
      hcx: false,
      gslb: false,
      level: "testapp",
      waf: false
    }; 
  
    stats: VcnStats = {
      countApply: 0,
      countCloud: 0,
      countDestroy: 0,
      countFactory: 0,
      countPks: 0,
      countPlan: 0,
      countVMC: 0
    }

    vcnfactoryprogress: Vcnprogress = {
      changes: 0,
      completed: false,
      lastline: "",
      fullline: ""
    };

    vcnfactorychanges: Vcnchanges = {
      changes: 0,
      tocreate: 0,
      toupdate: 0,
      todelete: 0,
      output: ""
    };

    vcncloudprogress: Vcnprogress = {
      changes: 0,
      completed: false,
      lastline: "",
      fullline: ""
    };

    vcncloudchanges: Vcnchanges = {
      changes: 0,
      tocreate: 0,
      toupdate: 0,
      todelete: 0,
      output: ""
    };

    vcnpksprogress: Vcnprogress = {
      changes: 0,
      completed: false,
      lastline: "",
      fullline: ""
    };

    vcnpkschanges: Vcnchanges = {
      changes: 0,
      tocreate: 0,
      toupdate: 0,
      todelete: 0,
      output: ""
    };

  logs: VcnLogs[] = [];
  vms_deploying = false;
  vms_loading = false;
  vms_type = "success";

  cloud_deploying = false;
  cloud_loading = false;
  cloud_type = "success";

  pks_deploying = false;
  pks_loading = false;
  pks_type = "success";

  constructor(private http: HttpClient, private auth: AuthenticationService, private _pushNotifications: PushNotificationsService) {
    this._pushNotifications.requestPermission();
  }


  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', Authorization: `Bearer ${this.auth.getToken()}`, 'User': `${this.auth.getUserDetails().username}` })
  };


  notify(text: string){ //our function to be called on click
    let options = { //set options
      body: text,
      icon: "assets/outline-vcn.png" //adding an icon
    }
     this._pushNotifications.create('VcnApp', options).subscribe( //creates a notification
        res => console.log(res),
        err => console.log(err)
    );
  }

  getAlerts(): Observable<any> {
    return this.http.post("/api/appdefense/list",
    "{\"token\":\"123\"}", this.httpOptions)
  };

  cacheVcnAlerts(vcnalerts: Vcnalert[]) {
    this.vcnalerts = vcnalerts;
  }

  getCachedVcnAlerts() {
    return this.vcnalerts;
  }
  
  getVcnConfig(): Observable<Vcnconfig> {
    return of(this.vcn);
  }

  getVcnEdges(vcnconfig: Vcnconfig): Observable<any> {
    return this.http.post("/api/sdwan/list", vcnconfig, this.httpOptions)
  }

  cacheVcnEdges(vcnedges: Vcnedge[]) {
    this.vcnedges = vcnedges;
  }

  getCachedVcnEdges() {
    return this.vcnedges;
  }

  getVcnChanges(vcnconfig: Vcnconfig): Observable<any> {
    this.vcn = vcnconfig;
    return this.http.post("/api/terraform/plan", vcnconfig, this.httpOptions)
  };

  followVcnChanges(vcnconfig: Vcnconfig) {
    this.vms_loading = true;
    this.getVcnChanges(vcnconfig).subscribe(vcnfactorychanges => {this.vcnfactorychanges = vcnfactorychanges; this.vms_loading = false;});
  }

  getVcnDestroyChanges(vcnconfig: Vcnconfig): Observable<any> {
    return this.http.post("/api/terraform/plan-destroy", vcnconfig, this.httpOptions)
  };

  getVcnProgress(): Observable<any> {
    return this.http.post("/api/terraform/progress", this.vcn, this.httpOptions)
  };

  followVcnProgress(vcnconfig: Vcnconfig) {
    const subscription = interval(1000).pipe(startWith(0), switchMap(() => this.getVcnProgress())).subscribe(
      {next: vcnfactoryprogress => {
          console.log("test service");
          this.vms_deploying = true;
          this.vcnfactoryprogress = vcnfactoryprogress;
          if (vcnfactoryprogress.completed)
          {
            this.vms_deploying = false;
            subscription.unsubscribe();
            this.vcnfactoryprogress.changes = this.vcnfactorychanges.changes;
            this.followVcnChanges(vcnconfig);
            this.notify("Factory Changes Applied!");
          }
      },
      error: err => console.error(err),
      complete: () => console.log('complete')
      });
  };

  postVcnConfig(vcnconfig: Vcnconfig): Observable<any> {
    this.vcn = vcnconfig;
    this.quarantine = [];
    console.log('postVcnConfig');
    return this.http.post("/api/terraform/apply", vcnconfig, this.httpOptions);
  };

  quarantineVcn(i: number): Observable<any> {
    this.vcn.quarantine = 1;
    this.vcnalerts[i].quarantine = true;
    this.quarantine[i] = true;
    return this.http.post("/api/terraform/apply",
    this.vcn, this.httpOptions)
  };

  destroyVcn(): Observable<any> {
    console.log('destroyVcnConfig');
    return this.http.post("/api/terraform/destroy",
    this.vcn, this.httpOptions)
  };

  // Cloud Methods

  getVcnCloudConfig(): Observable<Vcnconfig> {
    return of(this.vcncloud);
  }

  getVcnCloudChanges(vcnconfig: Vcnconfig): Observable<any> {
    this.vcncloud = vcnconfig;
    return this.http.post("/api/terraform/plan",
    vcnconfig, this.httpOptions)
  };

  followVcnCloudChanges(vcnconfig: Vcnconfig) {
    this.cloud_loading = true;
    this.getVcnCloudChanges(vcnconfig).subscribe(vcncloudchanges => {this.vcncloudchanges = vcncloudchanges; this.cloud_loading = false;});
  }

  getVcnCloudProgress(): Observable<any> {
    return this.http.post("/api/terraform/progress",
    this.vcncloud, this.httpOptions)
  };

  followVcnCloudProgress(vcnconfig: Vcnconfig) {
    const subscription = interval(1000).pipe(startWith(0), switchMap(() => this.getVcnCloudProgress())).subscribe(
      {next: vcncloudprogress => {
          console.log("test service");
          this.cloud_deploying = true;
          this.vcncloudprogress = vcncloudprogress;
          if (vcncloudprogress.completed)
          {
            this.cloud_deploying = false;
            subscription.unsubscribe();
            this.vcncloudprogress.changes = this.vcncloudchanges.changes;
            this.followVcnCloudChanges(vcnconfig);
            this.notify("Cloud Changes Applied!");
          }
      },
      error: err => console.error(err),
      complete: () => console.log('complete')
      });
  };

  postVcnCloudConfig(vcnconfig: Vcnconfig): Observable<any> {
    this.vcncloud = vcnconfig;
    return this.http.post("/api/terraform/apply",
    vcnconfig, this.httpOptions)
  };

  quarantineCloudVcn(): Observable<any> {
    this.vcncloud.quarantine = 1;
    return this.http.post("/api/terraform/apply",
    this.vcncloud, this.httpOptions)
  };

  destroyCloudVcn(): Observable<any> {
    return this.http.post("/api/terraform/destroy",
    this.vcncloud, this.httpOptions)
  };

  // PKS Methods

  getVcnPksConfig(): Observable<Vcnconfig> {
    return of(this.vcnpks);
  }

  getVcnPksChanges(vcnconfig: Vcnconfig): Observable<any> {
    this.vcnpks = vcnconfig;
    return this.http.post("/api/terraform/plan",
    vcnconfig, this.httpOptions)
  };

  followVcnPksChanges(vcnconfig: Vcnconfig) {
    this.pks_loading = true;
    this.getVcnPksChanges(vcnconfig).subscribe(vcnpkschanges => {this.vcnpkschanges = vcnpkschanges; this.pks_loading = false;});
  }

  getVcnPksProgress(): Observable<any> {
    return this.http.post("/api/terraform/progress",
    this.vcnpks, this.httpOptions)
  };

  followVcnPksProgress(vcnconfig: Vcnconfig) {
    const subscription = interval(1000).pipe(startWith(0), switchMap(() => this.getVcnPksProgress())).subscribe(
      {next: vcnpksprogress => {
          console.log("test service");
          this.pks_deploying = true;
          this.vcnpksprogress = vcnpksprogress;
          if (vcnpksprogress.completed)
          {
            this.pks_deploying = false;
            subscription.unsubscribe();
            this.vcnpksprogress.changes = this.vcnpkschanges.changes;
            this.followVcnPksChanges(vcnconfig);
            this.notify("PKS Changes Applied!");
          }
      },
      error: err => console.error(err),
      complete: () => console.log('complete')
      });
  };

  postVcnPksConfig(vcnconfig: Vcnconfig): Observable<any> {
    this.vcnpks = vcnconfig;
    return this.http.post("/api/terraform/apply",
    vcnconfig, this.httpOptions)
  };

  quarantinePksVcn(): Observable<any> {
    this.vcn.quarantine = 1;
    return this.http.post("/api/terraform/apply",
    this.vcnpks, this.httpOptions)
  };

  destroyPksVcn(): Observable<any> {
    return this.http.post("/api/terraform/destroy",
    this.vcnpks, this.httpOptions)
  };

    // VMC Methods

    getVcnVmcConfig(): Observable<Vcnconfig> {
      return of(this.vcnvmc);
    }
  
    getVcnVmcChanges(vcnconfig: Vcnconfig): Observable<any> {
      this.vcnvmc = vcnconfig;
      return this.http.post("/api/terraform/plan",
      vcnconfig, this.httpOptions)
    };
  
    getVcnVmcProgress(): Observable<any> {
      return this.http.post("/api/terraform/progress",
      this.vcnvmc, this.httpOptions)
    };
  
    postVcnVmcConfig(vcnconfig: Vcnconfig): Observable<any> {
      this.vcnvmc = vcnconfig;
      return this.http.post("/api/terraform/apply",
      vcnconfig, this.httpOptions)
    };
  
    quarantineVmcVcn(): Observable<any> {
      this.vcn.quarantine = 1;
      return this.http.post("/api/terraform/apply",
      this.vcnvmc, this.httpOptions)
    };
  
    destroyVmcVcn(): Observable<any> {
      return this.http.post("/api/terraform/destroy",
      this.vcnvmc, this.httpOptions)
    };

    migrateHcx(): Observable<any> {
      console.log('hcx post api');
      return this.http.post("/api/hcx",
      "{\"token\":\"123\"}", this.httpOptions)
    };

    getDashboard(): Observable<any> {
      console.log('dashboard post api');
      return this.http.post("/api/dashboard",
      "{\"token\":\"123\"}", this.httpOptions)
    };

    getLogs(): Observable<any> {
      console.log('logs post api');
      return this.http.post("/api/logs",
      "{\"token\":\"123\"}", this.httpOptions)
    };

    fakeOn(): Observable<any> {
      console.log('fake post api');
      return this.http.post("/api/fakeon",
      "{\"token\":\"123\"}", this.httpOptions)
    };

    fakeOff(): Observable<any> {
      console.log('fake post api');
      return this.http.post("/api/fakeoff",
      "{\"token\":\"123\"}", this.httpOptions)
    };

    // Schedule
    scheduleVcnConfig(vcnschedule: VcnSchedule): Observable<any> {
      console.log('scheduleVcnConfig');
      return this.http.post("/api/schedule/apply", vcnschedule, this.httpOptions);
    };

    getVcnSchedule(): Observable<any> {
      console.log('Get Vcn Schedule');
      return this.http.post("/api/schedule/list", this.vcnschedules, this.httpOptions);
    };

    cancelVcnSchedule(id: string): Observable<any> {
      console.log('Delete Vcn Schedule');
      return this.http.post("/api/schedule/delete/"+id, this.vcnschedules, this.httpOptions);
    };

    httpOptionsTmp = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json'})
    };
  
    getVcnFlows(): Observable<any> {
      return this.http.get("http://127.0.0.1:5001/flows", this.httpOptionsTmp);
    }

    startVcnFlows(): Observable<any> {
      return this.http.get("http://127.0.0.1:5001/kafka_start", this.httpOptionsTmp);
    }

    stopVcnFlows(): Observable<any> {
      return this.http.get("http://127.0.0.1:5001/kafka_stop", this.httpOptionsTmp);
    }
}
