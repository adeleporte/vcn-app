//import { VerticalNavGroupRegistrationService } from '@clr/angular/layout/vertical-nav/providers/vertical-nav-group-registration.service';

export class Vcnconfig {
    id?: string;
    application?: string;
    site?: string;
    vm?: boolean;
    networks?: boolean;
    security?: boolean;
    appdefense?: boolean;
    vrni?: boolean;
    threshold?: boolean;
    wan?: boolean;
    bandwidth?: number;
    token?: string;
    quarantine?: number;
    pks?: string;
    region?: string;
    sddc?: string;
    class?: string;
    servicegroup?: string;
    user?: string;
    replicas?: number;
    appversion?: string;
    avi?: boolean;
    hcx?: boolean;
    gslb?: boolean;
    level?: string;
    waf?: boolean;
  }

  export class Vcnchanges {
    changes: number;
    tocreate?: number;
    toupdate?: number;
    todelete?: number;
    output?: string;
  }

  export class Vcnprogress {
    changes: number;
    completed: boolean;
    lastline: string;
    fullline: string;
  }

  export class Vcnalerts {
    members: Vcnalert[];
    totalElements: number;
  }

  export class Vcnsite {
    id: string;
    city: string;
    lat: number;
    lon: number;
  }

  export class Vcnedge {
    id: string;
    name: string;
    modelNumber: string;
    site: Vcnsite;
  }
  
  export class Vcnalert {
    id: number;
    ruleViolated: string;
    severity: string;
    violationDetails: VcnalertDetails;
    quarantine: boolean;
  }

  export class VcnalertDetails {
    cli: string;
    processPath: string;
    localAddress: string;
    remoteAddress: string;
    localPort: string;
    remotePort: string;
  }

  export class VcnUser {
    username: string;
    password: string;
  }

  export class VcnLogs {
    date: string;
    task: string;
    cloud: string;
  }

  export class VcnStats {
      countFactory: number;
      countPks: number;
      countCloud: number;
      countVMC: number;
      countPlan: number;
      countApply: number;
      countDestroy: number;
  }

  export class VcnSchedule {
    id: number;
    action: string;
    completed: boolean;
    date: Date;
    config: Vcnconfig;
  }